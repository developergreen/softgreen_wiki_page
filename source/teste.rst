Cadastro
=====================================
O sistema Green é composto por quatro tipos de cadastros sendo eles: Usuário, Auxiliar, Pessoa e Produto. O cadastro é a base do sistema, todos os processos e lançamentos dependem das informações cadastradas. Portanto o bom funcionamento do sistema só é possível se o cadastro estiver completo.

Usuário
-------
Usuário - O sistema é iniciado com um usuário supervisor e a partir dele é cadastrado os demais usuários e suas autorizações de acesso e relatórios.

Auxiliar
^^^^^^^^^^^^^^^^
Auxiliar  -  O cadastro auxiliar é a base dos cadastros, e auxilia os cadastros de Pessoa e Produto.

Banco
"""""
Banco - O cadastro do banco já vem preenchido conforme a lista da febraban
link: https://portal.febraban.org.br

Defeito
"""""""
Defeito - Pode ser cadastrado tipos de defeitos dos produtos para o controle da produção. 

Causa
"""""
Causa - Pode ser cadastrado as causas  dos defeitos, para minimizá los na produção

Solução
"""""""
Solução - Pode ser cadastrado as soluções para facilitar na resolução das causas de defeitos.

Pessoa
^^^^^^
Pessoa - Pode ser cadastrado Pessoa fisica e juridica. Para simplificar é possível cadastrar cliente,  fornecedor,  funcionário entre outras opções na mesma base de cadastro. 

Pessoa Física
"""""""""""""

Pessoa Jurídica
"""""""""""""""

Produto
^^^^^^^
Produto - Pode ser cadastrado produto e serviços e suas especificações detalhadas como valores, marca, tamanho, peso e embalagem. 